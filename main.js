let number = document.querySelectorAll('.number, #dot');
let greyBG = document.getElementsByClassName('greyBG');
let sign = document.getElementsByClassName('sign');
let result = document.getElementById('result');
let inp = document.getElementById('inp');
let clearBtn = document.getElementById('cleare');
let toggleSign = document.getElementById('toggleSign');
let procentBtn = document.getElementById('procentBtn');


document.body.addEventListener('keydown', resultFromKeyEnter)

function resultFromKeyEnter(e) {
	if (e.keyCode == 13) {
		showResult()
	}
	if (e.keyCode == 8) {
		inp.value.substring(0, inp.value.length - 1);
	}
}


procentBtn.addEventListener('click', calcProcent);

function calcProcent() {

	for (i = 0; i < inp.value.length; i++) {
		if (isNaN(inp.value[i]) != true) {
			inp.value = inp.value / 100;
			break;
		}
	}

	if (inp.value == 'NaN') {
		inp.value = "";
	}
}


toggleSign.addEventListener('click', switchSign)

function switchSign() {

	for (i = 0; i < inp.value.length; i++) {
		if (isNaN(inp.value[i]) != true) {
			inp.value = inp.value * -1;
			break;
		}
	}

	if (inp.value == 'NaN') {
		inp.value = "";
	}
}



for (i = 0; i < number.length; i++) {
	number[i].addEventListener('click', inputNumber);

}

function inputNumber() {
	inp.value += this.innerHTML;

}
for (i = 0; i < sign.length; i++) {
	sign[i].addEventListener('click', inputSign)

}

function inputSign() {
	inp.value += this.innerHTML
}

result.addEventListener('click', showResult)

function showResult() {
	inp.value = eval(inp.value);
}
clearBtn.addEventListener('click', cleareResult)

function cleareResult() {
	inp.value = '';
}

inp.addEventListener('input', cleareInpValue)

function cleareInpValue(e) {
	if (isNaN(inp.value) == true && e.data != '+' && e.data != '-' && e.data != '/' && e.data != '*' && isNaN(e
			.data)) {
		inp.value = " ";
	}
}